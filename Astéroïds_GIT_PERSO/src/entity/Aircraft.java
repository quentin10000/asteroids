package entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Polygon;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Transform;

import spaceflight.Effect;
import spaceflight.Ressource;
import spaceflight.SpaceFlight;
import util.Timer;
import util.Vector2;

/**
 *
 * @author meryd
 */
public class Aircraft extends Sprite {

	/**
	 * Largeur du vaisseau spatial à l'écran en pixels.
	 */
	public static final int AIRCRAFT_WIDTH = 100;

	/**
	 * Hauteur du vaisseau spatial à l'écran en pixels.
	 */
	public static final int AIRCRAFT_HEIGHT = 100;

	/**
	 * Vitesse du vaisseau spatial.
	 */
	public static final int AIRCRAFT_SPEED = 256;

	/**
	 * Temps entre deux shoots.
	 */
	public static final int SHOOT_MILLIS = 500;

	/**
	 * Point de vie du vaisseau spatial.
	 */
	public static final int TOTAL_LIFE = 100;

	/**
	 * Vecteur de direction du vaisseau.
	 */
	private Vector2 dir;

	/**
	 * Vecteur de velocité du vaisseau.
	 */
	private Vector2 vel;

	/**
	 * Vecteur de déplacement du vaisseau.
	 */
	private Vector2 mov;

	/**
	 * Zone de vol du vaisseau.
	 */
	private Rectangle flightWindow;

	/**
	 * Liste des projectiles tirés par le vaisseau.
	 */
	private List<Shoot> shoots = new ArrayList<Shoot>();

	/**
	 * Liste des bonus effecté au vaisseau.
	 */
	private Map<Effect, Integer> effects = new HashMap<Effect, Integer>();

	/**
	 * Compte à rebour.
	 */
	private Timer timer;

	/**
	 * Temps écouler depuis la création du vaisseau.
	 */
	private int time = 0;

	/**
	 * Point de vie courrant du vaisseau.
	 */
	private int life = TOTAL_LIFE;

	/**
	 * Construit un nouveau vaisseau en coordonnées (0,0) sur l'écran.
	 */
	public Aircraft() {

		// Initialisation des vecteurs de déplacement
		dir = Vector2.newXY(0, 0);
		mov = Vector2.newXY(0, 0);
		vel = Vector2.newXY(AIRCRAFT_SPEED, AIRCRAFT_SPEED);

		flightWindow = new Rectangle(0, 0, SpaceFlight.GAME_WIDTH, SpaceFlight.GAME_HEIGHT);
		timer = new Timer(SHOOT_MILLIS);

		// On construit le polygone sur un carrÃ© de 1.0x1.0
		Polygon poly = new Polygon();
		poly.addPoint(0.5f, 0.0f);
		poly.addPoint(0.7f, 0.5f);
		poly.addPoint(1.0f, 0.5f);
		poly.addPoint(1.0f, 0.8f);
		poly.addPoint(0.6f, 0.8f);
		poly.addPoint(0.5f, 1.0f);
		poly.addPoint(0.4f, 0.8f);
		poly.addPoint(0.0f, 0.8f);
		poly.addPoint(0.0f, 0.5f);
		poly.addPoint(0.3f, 0.5f);

		// On zoome le polygone sur les vraies dimensions voulues
		int w = AIRCRAFT_WIDTH;
		int h = AIRCRAFT_HEIGHT;
		shape = poly.transform(Transform.createScaleTransform(w, h));
	}

	/**
	 * Effectue la mise à jour du vaisseau.
	 *
	 * @param gc
	 *            le conteneur de jeu.
	 * @param millis
	 *            millisecondes écoulées depuis la dernière mise à jour.
	 * @throws SlickException
	 */
	public void update(GameContainer gc, int millis) throws SlickException {
		if (updated) {
			move(millis, gc);
			ensureFlightWindow();

			// mise à jour des tirs
			Iterator<Shoot> itShoots = shoots.iterator();
			while (itShoots.hasNext()) {
				Shoot shoot = (Shoot) itShoots.next();
				if (shoot.getY() < 0) {
					itShoots.remove();
				} else {
					shoot.update(gc, millis);
				}
			}

			// mise à jour des bonus
			Iterator<Entry<Effect, Integer>> itEffect = effects.entrySet().iterator();
			while (itEffect.hasNext()) {
				Map.Entry<Effect, Integer> pair = (Map.Entry<Effect, Integer>) itEffect.next();
				int timeEffect = (int) pair.getValue() + 500;
				if (timeEffect < time) {
					itEffect.remove();
				}
			}

			timer.update(millis);
			time++;
		}
	}

	/**
	 * Effectue le rendu graphique du vaisseau et des tirs.
	 *
	 * @param gc
	 *            le conteneur de jeu.
	 * @param g
	 *            le contexte graphique.
	 * @throws SlickException
	 */
	public void render(GameContainer gc, Graphics g) throws SlickException {
		if (rendered) {

			float x = shape.getX();
			float y = shape.getY();
			Ressource.starship.draw(x, y, (float) AIRCRAFT_WIDTH / Ressource.starship.getWidth());

			for (Shoot shoot : shoots) {
				shoot.render(gc, g);
			}
			if (SpaceFlight.DEBUG) {
				g.draw(shape);
			}
		}
	}

	/**
	 * Effectue le déplacement du vaisseau depuis les entrées clavier.
	 *
	 * @param gc
	 *            le conteneur de jeu.
	 * @param delta
	 *            millisecondes écoulées depuis la dernière mise à jour.
	 * @throws SlickException
	 */
	public void move(int delta, GameContainer gc) {
		float sec = delta / 1000.0F;
		Input input = gc.getInput();

		dir.setXY(0f, 0f);

		if (input.isKeyDown(Input.KEY_LEFT)) {
			dir.setX(-1);
		}
		if (input.isKeyDown(Input.KEY_RIGHT)) {
			dir.setX(dir.getX() + 1);
		}
		if (input.isKeyDown(Input.KEY_UP)) {
			dir.setY(-1);
		}
		if (input.isKeyDown(Input.KEY_DOWN)) {
			dir.setY(dir.getY() + 1);
		}

		if (timer.isRinging() && input.isKeyDown(Input.KEY_SPACE)) {
			Shoot s = new Shoot((int) shape.getCenterX() - 2, (int) shape.getCenterY() - 70);
                        Shoot s1 = new Shoot((int) shape.getX() + 20, (int) shape.getCenterY() - 27);
                        Shoot s2 = new Shoot((int) shape.getX() + AIRCRAFT_WIDTH - 23, (int) shape.getCenterY() - 27);
			s.start();
			if (hasEffect(Effect.DOUBLE_SHOOT)) {
				
				s1.start();
				shoots.add(s1);
				
				s2.start();
				shoots.add(s2);
			}
                        if (hasEffect(Effect.LASER)) {
				s.setHeight((int) (SpaceFlight.GAME_HEIGHT - (SpaceFlight.GAME_HEIGHT - this.shape.getY())));
                                s1.setHeight((int) (SpaceFlight.GAME_HEIGHT - (SpaceFlight.GAME_HEIGHT - this.shape.getY())));
                                s2.setHeight((int) (SpaceFlight.GAME_HEIGHT - (SpaceFlight.GAME_HEIGHT - this.shape.getY())));
				s.setTexture(Ressource.laser);
                                s1.setTexture(Ressource.laser);
                                s2.setTexture(Ressource.laser);
				s.setY(0);
                                s1.setY(0);
                                s2.setY(0);
				timer.reset(0);
			} else {
				timer.reset(SHOOT_MILLIS);
			}
			shoots.add(s);
		}

		mov.setXY(dir.getX() * vel.getX() * sec, dir.getY() * vel.getY() * sec);
		translate(mov);
	}

	/**
	 * Définition de la fenètre de vol.
	 *
	 * @param x
	 *            Position en x de la fenètre de vol.
	 * @param y
	 *            Position en y de la fenètre de vol.
	 * @param width
	 *            Longueur de la fenètre de vol.
	 * @param height
	 *            Hauteur de la fenètre de vol.
	 */
	public void setFlightWindow(float x, float y, float width, float height) {
		flightWindow.setBounds(x, y, width, height);
	}

	/**
	 * Corrige la position du vaisseau en fonction de la fenètre de vol
	 */
	public void ensureFlightWindow() {
		if (shape.getX() < flightWindow.getX()) {
			locate(flightWindow.getX(), shape.getY());

		} else if (shape.getX() + AIRCRAFT_WIDTH > flightWindow.getMaxX()) {
			locate(flightWindow.getMaxX() - AIRCRAFT_WIDTH, shape.getY());

		} else if (shape.getY() < flightWindow.getY()) {
			locate(shape.getX(), flightWindow.getY());

		} else if (shape.getY() + AIRCRAFT_HEIGHT > flightWindow.getMaxY()) {
			locate(shape.getX(), flightWindow.getMaxY() - AIRCRAFT_HEIGHT);
		}
	}

	/**
	 * Détecte la collision entre un astéroïde et le vaisseau
	 *
	 * @param rock
	 *            Astéroïde sur lequel on test la collision.
	 * 
	 * @return True s'il y a collision <br>
	 *         False s'il y n'a pas collision
	 */
	public boolean collision(Rock rock) {
		return rock.intersects(shape);
	}

	/**
	 * Renvoie la liste des tirs
	 * 
	 * @return Liste des tirs
	 */
	public List<Shoot> getShoots() {
		return shoots;
	}

	/**
	 * Ajout de points de vie au vaisseau
	 *
	 * @param add
	 *            Point de vie à ajouter.
	 */
	public void addLife(int add) {
		this.life += add;
	}

	/**
	 * Enlever des points de vie au vaisseau
	 *
	 * @param min
	 *            Point de vie à enlever.
	 */
	public void minLife(int min) {
		this.life -= min;
	}

	/**
	 * Définir un nombre de points de vie au vaisseau
	 *
	 * @param life
	 *            Point de vie du vaisseau.
	 */
	public void setLife(int life) {
		this.life = life;
	}

	/**
	 * Renvoie le nombre du point de vie du vaisseau
	 * 
	 * @return nombre de point de vie
	 */
	public int getLife() {
		return life;
	}

	/**
	 * Ajout un bonus au vaisseau
	 *
	 * @param effect
	 *            Effet à ajouter
	 */
	public void addEffect(Effect effect) {
		this.effects.put(effect, time);
	}

	/**
	 * Enlever un bonus au vaisseau
	 *
	 * @param effect
	 *            Effet à enlever
	 */
	public void removeEffect(Effect effect) {
		this.effects.remove(effect);
	}

	/**
	 * Test si le vaisseau à l'effet
	 *
	 * @param effect
	 *            Effet à tester
	 * @return True : si le vaisseau à l'effet <br>
	 *         False : si le vaisseau n'a pas l'effet
	 */
	public boolean hasEffect(Effect effect) {
		return this.effects.containsKey(effect);
	}

	/**
	 * Renvoie tout les effets du vaisseau
	 * 
	 * @return phrase formatée de l'effet
	 */
	public String getEffect() {
		Iterator<Entry<Effect, Integer>> itEffect = effects.entrySet().iterator();
		while (itEffect.hasNext()) {
			Map.Entry<Effect, Integer> pair = (Map.Entry<Effect, Integer>) itEffect.next();
			return Ressource.languageRegistry("interface.bonus." + ((Effect) pair.getKey()).name());
		}
		return Ressource.languageRegistry("interface.bonus.AUCUN");

	}
}
