package entity;

import spaceflight.Ressource;

public class Explosion {
	
	/**
	 * Position de l'explosion
	 */
	private int x, y;
	
	/**
	 * Position de l'explosion sur la texture
	 */
	private int xc, yc = 0;
	
	/**
	 * Taille de l'explosion
	 */
	private float size;
	
	/**
	 * Création d'un explosion en (x, y) de taille size
	 *
	 * @param x
	 * 		position en x de l'explosion
	 * @param y
	 * 		position en y de l'explosion
	 * @param size
	 * 		taille de l'explosion
	 */
	public Explosion(int x, int y, int size) {
		this.x = x;
		this.y = y;
		this.size = size/15.0f;
	}
	
	/**
	 * Effectue la mise à jour de l'explosions.
	 *
	 * @param time
	 * 		millisecondes écoulées depuis la dernière mise à jour
	 */
	public void update(int time){
		if (time % 9 == 0) {
			if (yc % 6 == 0) {
				xc++;
				yc = 0;
			}
			yc++;
		}
		float w = 128.0f*size;
		float h = 72.0f*size;
		Ressource.explosion.draw(x-w, y-h, x + w, y + h, (xc-1)*512, (yc-1)*288, xc*512, yc*288);
	}
}
