package entity;

import java.util.Random;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Circle;
import spaceflight.Effect;
import spaceflight.Ressource;
import spaceflight.SpaceFlight;
import util.Vector2;

public class Rock extends Sprite {

	/**
	 * Taille d'un astéroïde
	 */
	private int size;

	/**
	 * Variable Ramdom utilisée pour la génération de l'astéroïde
	 */
	private Random rnd;

	/**
	 * Vitesse d'un astéroïde
	 */
	private float speed = 0;

	/**
	 * Bonus de l'astéroïde qui sera affecté au vaisseau après la destruction de
	 * l'astéroïde
	 */
	private Effect effect;

	/**
	 * Texture de l'astéroïde
	 */
	private Image texture;

	/**
	 * Angle de rotation de l'astéroïde
	 */
	private float angle;

	/**
	 * Point de vie de l'astéroïde
	 */
	private int life;

	/**
	 * Création d'un astéroïde en (x, y) de taille size
	 * 
	 * @param x
	 *            position en x de l'astéroïde
	 * @param y
	 *            position en y de l'astéroïde
	 * @param size
	 *            taille de l'astéroïde
	 */
	public Rock(int x, int y, int size) {
		shape = new Circle(x, y, size);
		this.size = size;
		rnd = new Random();
		speed = rnd.nextInt(20) / 10.0F + 0.2F;
		if (rnd.nextInt(100) >= 50) {
			texture = Ressource.asteroid;
		} else {
			texture = Ressource.asteroid2;
		}
		angle = rnd.nextInt(360);
		life = 1;
	}

	/**
	 * Augmentation de la vitesse de l'astéroïde
	 * 
	 * @param speed
	 *            point d'augmentation de la vitesse
	 */
	public void increaseSpeed(float speed) {
		this.speed += speed;
	}

	/**
	 * Effectue la mise à jour de l'astéroïde.
	 *
	 * @param gc
	 *            le conteneur de jeu.
	 * @param millis
	 *            millisecondes écoulées depuis la dernière mise à jour.
	 * @throws SlickException
	 */
	public void update(GameContainer gc, int millis) throws SlickException {
		if (updated) {
			translate(Vector2.newXY(0, speed));
		}
	}

	/**
	 * Effectue le rendu graphique de l'astéroïde.
	 *
	 * @param gc
	 *            le conteneur de jeu.
	 * @param g
	 *            le contexte graphique.
	 * @throws SlickException
	 */
	public void render(GameContainer gc, Graphics g) throws SlickException {
		float x = shape.getX();
		float y = shape.getY();
		if (rendered) {
			if (hasEffect()) {
				switch (getEffect()) {
				case LASER:
					Ressource.asteroid_effect_laser.draw(x, y,
							(float) shape.getWidth() / Ressource.asteroid_effect_laser.getWidth());
					break;

				case DOUBLE_SHOOT:
					Ressource.asteroid_effect_dshoot.draw(x, y,
							(float) shape.getWidth() / Ressource.asteroid_effect_dshoot.getWidth());
					break;

				case ADD_LIFE:
					Ressource.asteroid_effect_life.draw(x, y,
							(float) shape.getWidth() / Ressource.asteroid_effect_life.getWidth());
					break;
				}

			} else {
				// img.setCenterOfRotation(img.getWidth()/2-(95+size),
				// img.getHeight()/2-(95+size));
				// img.rotate(5f);
				float scale = shape.getWidth() / texture.getWidth();
				texture.setCenterOfRotation(texture.getWidth() * scale / 2, texture.getHeight() * scale / 2);
				texture.setRotation(angle);
				if (rnd.nextInt(100) < 30) {
					angle--;
				}
				texture.draw(x, y, scale);
				// angle = 0;

			}
			if (SpaceFlight.DEBUG) {
				g.draw(shape);
			}
		}
	}

	/**
	 * Réinitialise un l'astéroïde
	 */
	public void respawn() {
		removeEffect();
		respawn(rnd.nextInt(SpaceFlight.DISPLAY_WIDTH),
				rnd.nextInt(SpaceFlight.DISPLAY_HEIGHT) - SpaceFlight.DISPLAY_HEIGHT, rnd.nextInt(25) + 15);
		if (rnd.nextInt(100) < 3) {
			addEffect(Effect.randomEffect());
			respawn(rnd.nextInt(SpaceFlight.DISPLAY_WIDTH),
					rnd.nextInt(SpaceFlight.DISPLAY_HEIGHT) - SpaceFlight.DISPLAY_HEIGHT, 100);
			setLife(3);
		}
		angle = rnd.nextInt(360);
		this.increaseSpeed(0.05F);
	}

	/**
	 * Réinitialise un l'astéroïde en (x, y) et de taille size
	 * 
	 * @param x
	 *            position en x de l'astéroïde
	 * @param y
	 *            position en y de l'astéroïde
	 * @param size
	 *            taille de l'astéroïde
	 */
	public void respawn(int x, int y, int size) {
		shape = new Circle(x, y, size);
	}

	/**
	 * Defini la position y de l'astéroïde
	 * 
	 * @param y
	 *            position en y de l'astéroïde
	 */
	public void setY(int y) {
		shape.setY(y);
	}

	/**
	 * Donne la position en x de l'astéroïde
	 * 
	 * @return position en x de l'astéroïde
	 */
	public float getX() {
		return shape.getCenterX();
	}

	/**
	 * Donne la position en y de l'astéroïde
	 * 
	 * @return position en y de l'astéroïde
	 */
	public float getY() {
		return shape.getCenterY();
	}

	/**
	 * Donne la taille de l'astéroïde
	 * 
	 * @return taille de l'astéroïde
	 */
	public int getSize() {
		return size;
	}

	/**
	 * Détecte la collision entre un astéroïde et un tir
	 * 
	 * @param shoot
	 *            Tir avec lequel on test la collision
	 * @return
	 */
	public boolean collision(Shoot shoot) {
		return shoot.intersects(shape);
	}

	/**
	 * Supprime le bonus de l'astéroïde
	 */
	public void removeEffect() {
		this.effect = null;
	}

	/**
	 * Ajoute un bonus sur l'astéroïde
	 * 
	 * @param effect
	 *            Bonus à ajouter
	 */
	public void addEffect(Effect effect) {
		this.effect = effect;
	}

	/**
	 * Teste si l'astéroïde à un bonus
	 * 
	 * @return true : si l'astéroïde contient un bonus </br>
	 *         false: si l'astéroïde ne contient pas de bonus
	 */
	public boolean hasEffect() {
		return this.effect != null;
	}

	/**
	 * Donne le nom du bonus affecté à l'astéroïde
	 * 
	 * @return
	 */
	public Effect getEffect() {
		return this.effect;
	}

	/**
	 * Enleve des point de vie à l'astéroïde
	 * 
	 * @param min
	 *            point de vie à enlever
	 */
	public void minLife(int min) {
		life -= min;
	}

	/**
	 * Defini un nombre de point de vie de l'astéroïde
	 * 
	 * @param life
	 *            point de vie de l'astéroïde
	 */
	public void setLife(int life) {
		this.life = life;
	}

	/**
	 * Donne de nombre de point de vie de l'astéroïde
	 * 
	 * @return point de vie de l'astéroïde
	 */
	public int getLife() {
		return life;
	}

	/**
	 * Vérifie si l'astéroïde est ou pas cassé
	 * 
	 * @return true : si l'astéroïde n'a plus de point de vie </br>
	 *         false: si l'astéroïde a encore des point de vie
	 */
	public boolean isBreak() {
		return life <= 0;
	}
}
