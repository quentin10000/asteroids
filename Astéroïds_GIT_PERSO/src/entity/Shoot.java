package entity;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;

import spaceflight.Ressource;
import util.Vector2;

public class Shoot extends Sprite {
	/**
	 * Longueur en pixel du tir
	 */
	private int width;

	/**
	 * Hauteur en pixel du tir
	 */
	private int height;

	/**
	 * Texture du tir
	 */
	private Image texture = Ressource.shoot;

	/**
	 * Création d'un tir en (x,y)
	 * 
	 * @param x
	 *            position en x du tir
	 * @param y
	 *            position en y du tir
	 */
	public Shoot(int x, int y) {
		width = 5;
		height = 10;
		shape = new Rectangle(x, y, width, height);
	}

	/**
	 * Effectue la mise à jour d'un tir.
	 *
	 * @param gc
	 *            le conteneur de jeu.
	 * @param millis
	 *            millisecondes écoulées depuis la dernière mise à jour.
	 * @throws SlickException
	 */
	public void update(GameContainer gc, int millis) throws SlickException {
		if (updated) {
			translate(Vector2.newXY(0, -4));
		}
	}

	/**
	 * Effectue le rendu graphique d'un tir.
	 *
	 * @param gc
	 *            le conteneur de jeu.
	 * @param g
	 *            le contexte graphique.
	 * @throws SlickException
	 */
	public void render(GameContainer gc, Graphics g) throws SlickException {
		if (rendered) {
			// texture.draw(this.getX(), this.getY(), (float) 5f /
			// texture.getWidth());
			if (texture == Ressource.shoot) {
				texture.draw(this.getX(), this.getY(), (float) 5f / texture.getWidth());
			} else {
				texture.draw(this.getX(), this.getY(), width, height);
			}

		}
	}

	/**
	 * Defini la position y d'un tir
	 * 
	 * @param y
	 *            position en y d'un tir
	 */
	public void setY(int y) {
		shape.setY(y);
	}

	/**
	 * Donne la position en x d'un tir
	 * 
	 * @return position en x d'un tir
	 */
	public float getX() {
		return shape.getX();
	}

	/**
	 * Donne la position en y d'un tir
	 * 
	 * @return position en y d'un tir
	 */
	public float getY() {
		return shape.getY();
	}

	/**
	 * Definir la longueur du tir
	 * 
	 * @param width
	 *            longueur en pixel
	 */
	public void setWidth(int width) {
		this.width = width;
		shape = new Rectangle(this.getX(), this.getY(), width, height);
	}

	/**
	 * Definir la hauteur du tir
	 * 
	 * @param height
	 *            hauteur en pixel
	 */
	public void setHeight(int height) {
		this.height = height;
		shape = new Rectangle(this.getX(), this.getY(), width, height);
	}

	/**
	 * Definir la texture du tir
	 * 
	 * @param img
	 *            texture du tir
	 */
	public void setTexture(Image img) {
		this.texture = img;
	}

}
