package entity;

import org.newdawn.slick.geom.Shape;

import util.Vector2;

public class Sprite {

	/**
	 * Forme de l'entity
	 */
	protected Shape shape;

	/**
	 * Controle de la mise a jour de l'entity
	 */
	protected boolean updated;

	/**
	 * Controle du rendu graphique de l'entity
	 */
	protected boolean rendered;

	/**
	 * Teste l'état d'activation de la phase de mise à jour.
	 *
	 * @return true si et seulement si la phase de mise à jour est activée.
	 */
	public boolean isUpdated() {
		return updated;
	}

	/**
	 * Active ou inhibe la phase de mise à jour.
	 *
	 * @param updated
	 *            le nouvel état d'activation.
	 */
	public void setUpdated(boolean updated) {
		this.updated = updated;
	}

	/**
	 * Teste l'état d'activation de la phase de rendu graphique.
	 *
	 * @return true si et seulement si la phase de rendu graphique est activée.
	 */
	public boolean isRendered() {
		return rendered;
	}

	/**
	 * Active ou inhibe la phase de rendu graphique.
	 *
	 * @param rendered
	 *            le nouvel état d'activation.
	 */
	public void setRendered(boolean rendered) {
		this.rendered = rendered;
	}

	/**
	 * Active la phase cyclique de mise à jour + rendu graphique.
	 */
	public void start() {
		rendered = true;
		updated = true;
	}

	/**
	 * Inhibe la phase cyclique de mise à jour + rendu graphique.
	 */
	public void stop() {
		rendered = false;
		updated = false;
	}

	/**
	 * Localise le vaisseau spatial à des coordonnées spécifiques.
	 *
	 * @param x
	 *            la coordonnée horizontale souhaitée.
	 * @param y
	 *            la coordonnée verticale souhaitée.
	 */
	public void locate(float x, float y) {
		shape.setLocation(x, y);
	}

	/**
	 * Translate le vaisseau spatial par rapport à sa position courante.
	 * 
	 * @param mov
	 *            Vecteur de translation
	 */
	public void translate(Vector2 mov) {
		translate(mov.getX(), mov.getY());
	}

	/**
	 * Translate le vaisseau spatial par rapport à sa position courante.
	 *
	 * @param dx
	 *            la distance à ajouter à la composante horizontale courante.
	 * @param dy
	 *            la distance à ajouter à la composante verticale courante.
	 */
	public void translate(float dx, float dy) {
		float x = shape.getX();
		float y = shape.getY();
		shape.setLocation(x + dx, y + dy);
	}

	/**
	 * Teste s'il y a une collision avec une autre entity
	 * 
	 * @param other
	 *            autre entity a tester
	 * @return
	 */
	public boolean intersects(Shape other) {
		return shape.intersects(other);

	}

}
