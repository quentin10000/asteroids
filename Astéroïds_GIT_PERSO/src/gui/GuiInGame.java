package gui;

import java.util.Iterator;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

import entity.Aircraft;
import spaceflight.GenExplosion;
import spaceflight.Notif;
import spaceflight.Ressource;
import spaceflight.SpaceFlight;
import spaceflight.SpawnerRock;

public class GuiInGame extends Gui {

	/**
	 * Vaisseau du jeu
	 */
	private Aircraft aircraft;

	/**
	 * Générateur d'astéroïde
	 */
	private SpawnerRock spawnRock;

	/**
	 * Temps du jeu
	 */
	private int time;

	/**
	 * Création du systeme pour afficher les notifs
	 */
	private Notif notif = new Notif();

	/**
	 * Création du générateur d'explosions
	 */
	private GenExplosion explosions = new GenExplosion();

	/**
	 * Le jeu est en game over
	 */
	private boolean gameOver = false;

	/**
	 * Position y des image de fond
	 */
	private int y = 0;
	
	/**
	 * Création de l'interface en jeu
	 * 
	 * @param app
	 *            Contenu du jeu
	 */
	public GuiInGame(AppGameContainer app) {
		super(app);
	}

	@Override
	public void init(GameContainer gc) {
		aircraft = new Aircraft();
		spawnRock = new SpawnerRock();
		gameOver = false;
		SpaceFlight.setScore(0);
		aircraft.locate(0.5f * (gc.getWidth() - Aircraft.AIRCRAFT_WIDTH), (SpaceFlight.GAME_HEIGHT * 75) / 100 + 20);
		aircraft.start();
		aircraft.setFlightWindow((SpaceFlight.GAME_WIDTH - (SpaceFlight.GAME_WIDTH * 90) / 100) / 2,
				(SpaceFlight.GAME_HEIGHT * 75) / 100, (SpaceFlight.GAME_WIDTH * 90) / 100,
				(SpaceFlight.GAME_HEIGHT * 25) / 100);

		this.addButton(1, getGame().getWidth() / 2 - Ressource.guiButtonPauseBase.getWidth() / 2, 300, 1,
				Ressource.guiButtonPauseBase, Ressource.guiButtonPauseHover);
		this.addButton(2, getGame().getWidth() / 2 - Ressource.guiButtonMainBase.getWidth() / 2, 400, 1,
				Ressource.guiButtonMainBase, Ressource.guiButtonMainHover);
	}

	@Override
	public void update(GameContainer gc, int delta) throws SlickException {
		notif.update();
		explosions.update();
		if (!SpaceFlight.isPaused()) {
			aircraft.update(gc, delta);
			spawnRock.update(gc, delta, aircraft);
			if (!gameOver && time % 30 == 0) {
				SpaceFlight.addScore(1);
			}
		} else {
			Iterator<GuiButton> active = this.getActiveButton();

			while (active.hasNext()) {
				GuiButton button = (GuiButton) active.next();

				int id = button.getId();

				if (id == 1) {
					SpaceFlight.setPause(false);
				} else if (id == 2) {
					GuiStart.setLaunchGame(false);
				}
			}
		}
		Input input = gc.getInput();

		if (aircraft.getLife() == 0) {
			gameOver = true;
		}

		if (gameOver) {
			spawnRock.stop();
			aircraft.stop();
		}

		if (input.isKeyPressed(Input.KEY_F3)) {
			SpaceFlight.DEBUG = !SpaceFlight.DEBUG;
		}

		if (gameOver && input.isKeyDown(Input.KEY_R)) {
			init(gc);
		}

		if (input.isKeyPressed(Input.KEY_ESCAPE)) {
			// pause = !pause;
			SpaceFlight.setPause(!SpaceFlight.isPaused());
		}
		time++;
		super.update(gc, delta);
	}


	@Override
	public void render(GameContainer gc, Graphics g) throws SlickException {
		Ressource.bg.draw(0, y, SpaceFlight.GAME_WIDTH, SpaceFlight.GAME_HEIGHT);
		Ressource.bg.draw(0, y - SpaceFlight.GAME_HEIGHT, SpaceFlight.GAME_WIDTH, SpaceFlight.GAME_HEIGHT);

		if (y > SpaceFlight.GAME_HEIGHT) {
			y = 0;
		}

		y++;

		int gameW = SpaceFlight.GAME_WIDTH;
		int gameH = SpaceFlight.GAME_HEIGHT;
		int gameX = (int) (0.5f * (SpaceFlight.DISPLAY_WIDTH - gameW));
		int gameY = SpaceFlight.DISPLAY_HEIGHT - SpaceFlight.GAME_HEIGHT;
		g.setClip(gameX, gameY, gameW, gameH);
		aircraft.render(gc, g);
		spawnRock.render(gc, g);

		if (gameOver) {
			int margeTop = 290;

			Ressource.lemonMick.drawString(SpaceFlight.DISPLAY_WIDTH / 2 - 120, margeTop, "Game Over", Color.red);

			String score = "Score final : " + SpaceFlight.getScore();
			Ressource.lemonMick.drawString(SpaceFlight.DISPLAY_WIDTH / 2 - (score.length() / 2 * 22), margeTop + 50,
					score);
			Ressource.lemonMick.drawString(SpaceFlight.DISPLAY_WIDTH / 2 - 200, margeTop + 100, "Press R to restart");

		} else {
			Ressource.lemonMick.drawString(90, 715, aircraft.getLife() * 100 / Aircraft.TOTAL_LIFE + "%");
			Ressource.icon_life.draw(30, 710, (float) 100 / Ressource.asteroid.getWidth());

			Ressource.icon_effect.draw(0, 630, (float) 100 / Ressource.asteroid.getWidth());
			Ressource.lemonMick.drawString(90, 642, aircraft.getEffect() + "");

			Ressource.displayAnimation(20, 20, time, 100F);
			Ressource.lemonMick.drawString(120, 40, "" + SpaceFlight.getScore());
		}

		if (SpaceFlight.DEBUG) {
			gc.setShowFPS(true);
			g.setColor(Color.white);
			g.drawString("Life : " + aircraft.getLife(), 10.0f, 30.0f);
			g.drawString("Score : " + SpaceFlight.getScore(), 10.0f, 50.0f);
			g.drawString("Time : " + time, 10.0f, 70.0f);
		}
		notif.render(gc);
		explosions.render(gc);

		if (SpaceFlight.isPaused()) {
			g.setColor(new Color(0.1F, 0.1F, 0.1F, 0.7F));
			g.fillRect(0, 0, SpaceFlight.DISPLAY_WIDTH, SpaceFlight.DISPLAY_HEIGHT);
			super.render(gc, g);
		}
	}
}
