package main;

import org.newdawn.slick.SlickException;
import spaceflight.SpaceFlight;

/**
 *
 * @author meryd
 */
public class Main {

    /** 
     * @param args the command line arguments
     */
    public static void main(String[] args) throws SlickException {
        SpaceFlight.start();
    }
}
