package spaceflight;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public enum Effect {
	LASER,
	ADD_LIFE,
	DOUBLE_SHOOT;
	
	/**
	 * Liste de tous les effets
	 */
	private static final List<Effect> VALUES = Collections.unmodifiableList(Arrays.asList(values()));
	
	/**
	 * Taille de la liste de tous les effets
	 */
	private static final int SIZE = VALUES.size();
	
	/**
	 * Object Random
	 */
	private static final Random RANDOM = new Random();

	/**
	 * Sélectionne un effet random parmit la liste
	 */
	public static Effect randomEffect() {
		return VALUES.get(RANDOM.nextInt(SIZE));
	}
}
