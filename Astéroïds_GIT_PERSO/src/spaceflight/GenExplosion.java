package spaceflight;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.newdawn.slick.GameContainer;

import entity.Explosion;

public class GenExplosion {
	
	/**
	 * Temps écouler depuis la création du générateur d'explosions.
	 */
	private static int time;
	
	/**
	 * Liste de toutes les explosions
	 */
	private static Map<Integer, Explosion> explosions = new HashMap<Integer, Explosion>();
	
	/**
	 * Ajoute une explosion en (x, y) de taille size
	 * @param x
	 * 		Position en x de l'explosion
	 * @param y
	 * 		Position en y de l'explosion
	 * @param size
	 * 		taille de l'explosion
	 */
	public static void createEx(int x, int y, int size) {
		explosions.put(time, new Explosion(x, y, size));
	}
	
	/**
	 * Effectue la mise à jour des explosions.
	 */
	public void update() {
		time++;
	}
	
	/**
	 * Effectue le rendu graphique des explosions
	 * @param gc
	 * 		le conteneur de jeu.
	 */
	public void render(GameContainer gc){
		Iterator<Entry<Integer, Explosion>> itEffect = explosions.entrySet().iterator();
		
		while (itEffect.hasNext()) {
			Map.Entry<Integer, Explosion> pair = (Map.Entry<Integer, Explosion>)itEffect.next();
			int timeEx = (int) pair.getKey() + 50;
			Explosion e = (Explosion) pair.getValue();
			e.update(time);
			
			if (timeEx < time) {
				itEffect.remove();
			}
		}
		time++;
	}
}
