package spaceflight;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.newdawn.slick.GameContainer;

public class Notif {
	
	/**
	 * Temps écouler depuis la création du générateur d'explosions.
	 */
	private static int time;
	
	/**
	 * Liste de toutes les notifications
	 */
	private static Map<String, Integer> notifs = new HashMap<String, Integer>();
	
	/**
	 * Position en y dans notifs
	 */
	private static ArrayList<Integer> notifs_y = new ArrayList<Integer>();
	
	/**
	 * Creation d'une notifications
	 * @param text
	 * 		texte de la notification
	 */
	public static void createNotif(String text) {
		notifs.put(text, time);
		notifs_y.add(-50);
	}
	
	/**
	 * Mise a jour de la notif
	 */
	public void update() {
		time++;
	}
	
	/**
	 * Effectue le rendu graphique de la notification
	 * @param c
	 * 		le conteneur de jeu.
	 */
	public void render(GameContainer c){
		Iterator<Entry<String, Integer>> itEffect = notifs.entrySet().iterator();
		
		int i = 0;
		
		while (itEffect.hasNext()) {
			Map.Entry<String, Integer> pair = (Map.Entry<String, Integer>)itEffect.next();
			int timeNotif = (int) pair.getValue() + 300;
			int y = notifs_y.get(i);
			if (y < 30) {
				notifs_y.set(i, y+2);
			}
			if (timeNotif < time) {
				notifs_y.set(i, y-2);
			}
			if (y < -50) {
				notifs_y.remove(i);
				itEffect.remove();
				i--;
			}
			if (i > 0) {
				y += 10*i;
			}
			String text = (String) pair.getKey();
			float textWidth = Ressource.lemonMick.getWidth(text);
			Ressource.icon_notif.draw(c.getWidth()/2f - textWidth/2f - 65, y*(i+1), 0.2f);
			Ressource.lemonMick.drawString(c.getWidth()/2f - textWidth/2f, y*(i+1), text);
			i++;
		}
	}

}
