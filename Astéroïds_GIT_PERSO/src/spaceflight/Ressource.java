package spaceflight;

import java.util.HashMap;
import java.util.Map;

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;

public class Ressource {

	public static Image hightech01;
	public static Image hightech02;
	public static Image hightech03;
	public static Image hightech04;

	public static Image asteroid;
	public static Image asteroid2;
	public static Image asteroid_effect_laser;
	public static Image asteroid_effect_dshoot;
	public static Image asteroid_effect_life;
	
	public static Image bg;
	public static Image icon_life;
	public static Image icon_effect;
	public static Image icon_notif;

	public static Image reactor;
	public static Image explosion;

	public static Image starship;
	public static Image shoot;
	public static Image laser;

	public static Image androids;
	public static Image guiMainMenu;
	public static Image guiButtonPlayBase;
	public static Image guiButtonPlayHover;
	public static Image guiButtonCloseBase;
	public static Image guiButtonCloseHover;
	public static Image guiButtonPauseBase;
	public static Image guiButtonPauseHover;
	public static Image guiButtonMainBase;
	public static Image guiButtonMainHover;
	
	public static UnicodeFont lemonMick;
	
	/**
	 * Chargement des textures
	 * 
	 * @throws SlickException
	 * 		Impossible de charger les textures
	 */
	public static void loadTexture() {
		try {
			
			hightech01 = new Image("ressources/hightech01.png");
			hightech02 = new Image("ressources/hightech02.png");
			hightech03 = new Image("ressources/hightech03.png");
			hightech04 = new Image("ressources/hightech04.png");
			
			asteroid = new Image("ressources/meteor1.png");
			asteroid2 = new Image("ressources/meteor2.png");
			asteroid_effect_laser = new Image("ressources/planet1.png");
			asteroid_effect_dshoot = new Image("ressources/planet2.png");
			asteroid_effect_life = new Image("ressources/planet3.png");
			
			bg = new Image("ressources/background.png");
			icon_life = new Image("ressources/shield.png");
			icon_effect = new Image("ressources/effect.png");
			icon_notif  = new Image("ressources/info.png");

			reactor  = new Image("ressources/reactor.png");
			explosion  = new Image("ressources/explosion.png");

			starship = new Image("ressources/starship.png");
			shoot = new Image("ressources/shoot.png");
			laser = new Image("ressources/laser2.png");

			androids = new Image("ressources/androids.png");
			guiMainMenu = new Image("ressources/guiMainMenu.png");
			
			guiButtonPlayBase = new Image("ressources/play.png");
			guiButtonPlayHover = new Image("ressources/play_hover.png");
			
			guiButtonCloseBase = new Image("ressources/close.png");
			guiButtonCloseHover = new Image("ressources/close_hover.png");
			
			guiButtonPauseBase = new Image("ressources/pause.png");
			guiButtonPauseHover = new Image("ressources/pause_hover.png");
			
			guiButtonMainBase = new Image("ressources/mainMenu.png");
			guiButtonMainHover = new Image("ressources/mainMenu_hover.png");
			
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Chargement de la police de caractères
	 * 
	 * @throws SlickException
	 * 		Impossible de charger la police de caractères
	 */
	@SuppressWarnings("unchecked")
	public static void loadFont(){
		try {
			lemonMick = new UnicodeFont("ressources/LemonMilk.ttf", 40, false, false);
			lemonMick.addAsciiGlyphs();
			lemonMick.addGlyphs(400, 600);
			lemonMick.getEffects().add(new ColorEffect());
			lemonMick.loadGlyphs();
			
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Effectue le rendu graphique de l'animation pour afficher le score
	 * @param x
	 * 		positions en x de l'animation
	 * @param y
	 * 		positions en y de l'animation
	 * @param time
	 * 		temps courrant du jeu
	 * @param size
	 * 		taille de l'animation
	 */
	public static void displayAnimation(int x, int y, int time, float size){
		float scale = (float) size/hightech01.getWidth();
		
		hightech01.draw(x, y, scale);
		hightech01.setCenterOfRotation(hightech01.getWidth()*scale/2, hightech01.getHeight()*scale/2);
		hightech01.setRotation(time*0.5f);
		
		hightech02.draw(x, y, scale);
		hightech02.setCenterOfRotation(hightech02.getWidth()*scale/2, hightech02.getHeight()*scale/2);
		hightech02.setRotation(-time);
		
		hightech03.draw(x, y, scale);
		hightech03.setCenterOfRotation(hightech03.getWidth()*scale/2, hightech03.getHeight()*scale/2);
		hightech03.setRotation(time);
		
		hightech04.draw(x, y, scale);
		hightech04.setCenterOfRotation(hightech04.getWidth()*scale/2, hightech04.getHeight()*scale/2);
		hightech04.setRotation(-time);
	}
	
	/**
	 * Effectue le remplacement des texte de debug par une phrase
	 * 
	 * @param ref
	 * 		reference du texte a afficher
	 * @return
	 * 		phrase final
	 */
	public static String languageRegistry(String ref){
		return languageRegistry(ref, "");
	}
	
	/**
	 *  Effectue le remplacement des texte de debug par une phrase
	 *  
	 * @param ref
	 * 		reference du texte a afficher
	 * @param comp
	 * 		ajout d'un texte a la fin de la phrase final
	 * @return
	 * 		phrase final
	 */
	public static String languageRegistry(String ref, String comp){
		Map<String, String> language = new HashMap<String, String>();
		language.put("notif.bonus.LASER", "Bonus laser");
		language.put("notif.bonus.DOUBLE_SHOOT", "Bonus double tir");
		language.put("notif.bonus.ADD_LIFE", "Réparation du vaisseaux");
		language.put("notif.welcome", "Bienvenue à bord");
		language.put("interface.bonus.LASER", "Laser");
		language.put("interface.bonus.DOUBLE_SHOOT", "Double tir");
		language.put("interface.bonus.AUCUN", "Aucun");
		return (language.get(ref) == null ? "" : language.get(ref)) + comp;
	}
}
