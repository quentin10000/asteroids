package spaceflight;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import gui.Gui;
import gui.GuiStart;

/**
 *
 * @author meryd
 */
public class SpaceFlight extends BasicGame {

	/**
	 * Longueur en pixel de la fenetre
	 */
	public static final int DISPLAY_WIDTH = 1300;

	/**
	 * Hauteur en pixel de la fenetre
	 */
	public static final int DISPLAY_HEIGHT = 800;

	/**
	 * Limite de fps
	 */
	public static final int FRAME_RATE = 60;

	/**
	 * Nom de la fenetre
	 */
	public static final String GAME_TITLE = "SpaceFlight";

	/**
	 * Longueur en pixel du jeu
	 */
	public static final int GAME_WIDTH = DISPLAY_WIDTH;

	/**
	 * Hauteur en pixel du jeu
	 */
	public static final int GAME_HEIGHT = DISPLAY_HEIGHT;

	/**
	 * Affichage des éléments de debug
	 */
	public static boolean DEBUG = false;

	/**
	 * Le jeu est en pause
	 */
	private static boolean pause = true;

	/**
	 * Score du jeu
	 */
	private static int score = 0;

	/**
	 * Menu du principal
	 */
	private static Gui gui;

	/**
	 * Contenu du jeu
	 */
	private static AppGameContainer app;

	/**
	 * Initialisation du jeu
	 */
	SpaceFlight() {
		super(GAME_TITLE);
	}

	/**
	 * Lancement du jeu
	 * 
	 * @throws SlickException
	 */
	public static void start() throws SlickException {
		app = new AppGameContainer(new SpaceFlight());
		app.setDisplayMode(DISPLAY_WIDTH, DISPLAY_HEIGHT, false);
		app.setAlwaysRender(true);
		app.setIcons(new String[] { "ressources/icon16.png", "ressources/icon32.png" });
		gui = new GuiStart(app);
		app.start();
	}

	@Override
	public void init(GameContainer gc) throws SlickException {
		Ressource.loadTexture();
		Ressource.loadFont();
		gui.init(gc);
		gc.setTargetFrameRate(FRAME_RATE);
	}

	@Override
	public void update(GameContainer gc, int delta) throws SlickException {
		gui.update(gc, delta);
	}

	@Override
	public void render(GameContainer gc, Graphics g) throws SlickException {
		// g.setAntiAlias(true);
		gc.setShowFPS(false);
		gui.render(gc, g);
	}

	/**
	 * Ajout de point de score
	 * 
	 * @param add
	 *            Point de score à ajouter
	 */
	public static void addScore(int add) {
		score += add;
	}

	/**
	 * Definir un score
	 * 
	 * @param set
	 *            valeur du score
	 */
	public static void setScore(int set) {
		score = set;
	}

	/**
	 * Donne la valeur du score
	 * 
	 * @return Valeur de score
	 */
	public static int getScore() {
		return score;
	}

	/**
	 * Mettre le jeux en pause
	 * 
	 * @param bool
	 *            mettre ou pas en pause
	 */
	public static void setPause(boolean bool) {
		pause = bool;
	}

	/**
	 * Teste si le jeux est en pause ou pas
	 * 
	 * @return True : jeu en pause <br>
	 *         False : jeu n'est pas en pause
	 */
	public static boolean isPaused() {
		return pause;
	}
}
