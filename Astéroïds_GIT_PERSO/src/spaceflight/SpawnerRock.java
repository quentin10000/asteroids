package spaceflight;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

import entity.Aircraft;
import entity.Rock;
import entity.Shoot;

public class SpawnerRock {

	/**
	 * Nombre max d'astroïdes
	 */
	private static final int MAX_ROCK = 50;

	/**
	 * Liste des astroïdes crées
	 */
	private List<Rock> rocks = new ArrayList<Rock>();

	/**
	 * Variable Random utilisée pour la génération des astroïdes
	 */
	private Random rnd;

	/**
	 * Création du générateur en ajoutant les astroïdes dans le jeu
	 */
	public SpawnerRock() {
		rnd = new Random();
		for (int i = 0; i < MAX_ROCK; i++) {
			Rock r = new Rock(rnd.nextInt(SpaceFlight.DISPLAY_WIDTH), rnd.nextInt(SpaceFlight.DISPLAY_HEIGHT) - 200,
					rnd.nextInt(15) + 15);
			r.start();
			rocks.add(r);
		}
	}

	/**
	 * Effectue la mise à jour des astroïdes.
	 *
	 * @param gc
	 *            le conteneur de jeu.
	 * @param delta
	 *            millisecondes écoulées depuis la dernière mise à jour.
	 * @param aircraft
	 *            instance du vaisseau
	 * @throws SlickException
	 */
	public void update(GameContainer gc, int delta, Aircraft aircraft) throws SlickException {
		Iterator<Rock> itRocks = rocks.iterator();
		while (itRocks.hasNext()) {
			Rock rock = (Rock) itRocks.next();
			Iterator<Shoot> itShoot = aircraft.getShoots().iterator();
			while (itShoot.hasNext()) {
				Shoot shoot = (Shoot) itShoot.next();
				if (rock.collision(shoot)) {
					SpaceFlight.addScore(50);

					rock.minLife(1);
					if (rock.isBreak()) {
						if (rock.hasEffect()) {
							Notif.createNotif(Ressource.languageRegistry("notif.bonus." + rock.getEffect().name()));
							if (rock.getEffect() == Effect.ADD_LIFE) {
								aircraft.addLife(5);
							} else {
								aircraft.addEffect(rock.getEffect());
							}
						}
						GenExplosion.createEx((int) rock.getX(), (int) rock.getY(), rock.getSize());
						rock.respawn();
					}
					itShoot.remove();
				}
			}
			rock.update(gc, delta);
			if (rock.getY() > SpaceFlight.DISPLAY_HEIGHT + rock.getSize() / 2) {
				rock.respawn();
			}
			if (aircraft.collision(rock)) {
				aircraft.minLife(1);
				rock.respawn();
			}
		}
	}

	/**
	 * Effectue le rendu graphique des astroïdes
	 *
	 * @param gc
	 * 		le conteneur de jeu.
	 * @param g
	 * 		le contexte graphique.
	 * @throws SlickException
	 */
	public void render(GameContainer gc, Graphics g) throws SlickException {
		for (Rock rock : rocks) {
			rock.render(gc, g);
		}
	}

	/**
	 * Vidage de la liste d'astroïdes
	 */
	public void stop() {
		rocks = new ArrayList<Rock>();
	}

}
