package util;

/**
 * Le but de cette classe est de modéliser un chronomètre qui compte un
 * écoulement de temps en millisecondes et fait retentir une alarme lorsque le
 * temps écoulé devient égal ou supérieur à une durée initiale fixée.
 * 
 * @author meryd
 */
public class Timer {

	/**
	 * Nombre de millisecondes comptées depuis la création du chronomètre.
	 */
	private int ticks;

	/**
	 * Nombre de millisecondes à compter avant que l'alarme retentisse.
	 */
	private int delay;

	/**
	 * Construit un nouveau chronomàtre avec une durée d'alarme initiale.
	 * 
	 * @param millis
	 *            la durée d'alarme initiale du chronomètre en millisecondes.
	 */
	public Timer(int millis) {
		ticks = 0;
		setDelay(millis);
	}

	/**
	 * Setter privé pour vérifier la validité du paramètre millis
	 * 
	 * @param millis
	 *            delay en milliseconde
	 */
	private void setDelay(int millis) {
		delay = (millis < 1) ? 1 : millis;
	}

	/**
	 * Indique le temps compté par le chronomètre courant.
	 * 
	 * @return le nombre de millisecondes comptées par le chronomètre courant
	 *         depuis sa création.
	 */
	public int getTicks() {
		return ticks;
	}

	/**
	 * Indique la durée d'alarme du chronomètre courant.
	 * 
	 * @return le nombre de millisecondes à compter avant de déclencher
	 *         l'alarme.
	 */
	public int getDelay() {
		return delay;
	}

	/**
	 * Teste si l'alarme du chronomètre courant retentit.
	 * 
	 * @return true si et seulement si l'alarme retentit.
	 */
	public boolean isRinging() {
		return ticks >= delay;
	}

	/**
	 * Met à jour le nombre de millisecondes comptées par le chronomètre
	 * courant.
	 * 
	 * @param millis
	 *            le nombre (>= 0) de millisecondes à ajouter.
	 */
	public void update(int millis) {
		if (millis > 0) {
			ticks += millis;
		}
	}

	/**
	 * Recommence un nouveau cycle de comptage avec une nouvelle durée d'alarme.
	 * Cette opération est sans effet sur un cycle en cours.
	 * 
	 * @param millis
	 *            nouvelle durée d'alarme en millisecondes (>= 1).
	 */
	public void reset(int millis) {
		if (isRinging()) {
			ticks = 0;
			setDelay(millis);
		} else {
			// rien
		}
	}

	/**
	 * Recommence un nouveau cycle de comptage avec la même durée d'alarme.
	 * Cette opération est sans effet sur un cycle en cours.
	 */
	public void rearm() {
		reset(delay);
	}
}