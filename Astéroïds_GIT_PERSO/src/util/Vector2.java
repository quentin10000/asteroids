package util;

/**
 * Cette classe modèlise le concept mathèmatique de vecteur libre en deux
 * dimensions. Un vecteur libre est un vecteur qui n'est lié ni par une droite
 * (vecteur glissant), ni par un point d'application (vecteur liè).
 *
 * @author meryd
 */
public class Vector2 {

	/**
	 * Composante horizontale en coordonnées cartésiennes.
	 */
    private float x;

    /**
     * Composante verticale en coordonnées cartèsiennes.
     */
    private float y;

    /**
     * Constructeur privé pour forcer l'utilisation des forges statiques.
     */
    private Vector2() {
    }

    /**
     * Construit un nouveau vecteur à partir de coordonnèes cartésiennes.
     *
     * @param x la composante horizontale initiale.
     * @param y la composante verticale initiale.
     * @return le nouveau vecteur de coordonnèes cartèsiennes (x,y).
     */
    public static Vector2 newXY(float x, float y) {
        Vector2 v = new Vector2();
        v.setXY(x, y);
        return v;
    }

    /**
     * Construit un nouveau vecteur à partir de coordonnées polaires.
     *
     * @param radius le rayon initial.
     * @param degrees l'angle initial en degrés.
     * @return le nouveau vecteur de coordonnèes polaires (radius,degrees).
     */
    public static Vector2 newPolar(float radius, float degrees) {
        Vector2 v = new Vector2();
        v.setPolar(radius, degrees);
        return v;
    }

    /**
     * Indique la composante horizontale du vecteur courant en coordonnées
     * cartésiennes.
     *
     * @return la composante horizontale courante.
     */
    public float getX() {
        return x;
    }

    /**
     * Redéfinit la composante horizontale du vecteur courant en coordonnées
     * cartèsiennes.
     *
     * @param x la nouvelle composante horizontale.
     */
    public void setX(float x) {
        this.x = x;
    }

    /**
     * Indique la composante verticale du vecteur courant en coordonnées
     * cartésiennes.
     *
     * @return la composante verticale courante.
     */
    public float getY() {
        return y;
    }

    /**
     * Redéfinit la composante verticale du vecteur courant en coordonnées
     * cartésiennes.
     *
     * @param y la nouvelle composante verticale.
     */
    public void setY(float y) {
        this.y = y;
    }

    /**
     * Redéfinit le vecteur courant en utilisant des coordonnées cartésiennes.
     *
     * @param x la nouvelle composante horizontale.
     * @param y la nouvelle composante verticale.
     */
    public void setXY(float x, float y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Indique l'angle en degrés du vecteur courant en coordonnèes polaires.
     *
     * @return l'angle courant en degrès.
     */
    public float getAngle() {
        return (float) Math.toDegrees(Math.atan2(y, x));
    }

    /**
     * Rèdèfinit l'angle du vecteur courant en coordonnèes polaires.
     *
     * @param degrees le nouvel angle en dègrès en coordonnèes polaires.
     */
    public void setAngle(float degrees) {
        setPolar((float) Math.sqrt(x * x + y * y), degrees);
    }

    /**
     * Indique le carrè du rayon du vecteur courant en coordonnèes polaires.
     *
     * @return le carrè du rayon du vecteur courant en coordonnèes polaires.
     */
    public float getRadiusSqr() {
        return x * x + y * y;
    }

    /**
     * Indique le rayon du vecteur courant en coordonnèes polaires.
     *
     * @return le rayon courant.
     */
    public float getRadius() {
        return (float) Math.sqrt(x * x + y * y);
    }

    /**
     * Redèfinit le rayon du vecteur courant en coordonnèes polaires.
     *
     * @param radius le nouveau rayon du vecteur courant.
     */
    public void setRadius(float radius) {
        double t = Math.atan2(y, x);
        x = (float) (radius * Math.cos(t));
        y = (float) (radius * Math.sin(t));
    }

    /**
     * Redèfinit le vecteur courant en utilisant des coordonnèes polaires.
     *
     * @param radius le nouveau rayon du vecteur courant.
     * @param degrees le nouvel angle en degrès du vecteur courant.
     */
    public void setPolar(float radius, float degrees) {
        double t = Math.toRadians(degrees);
        x = (float) (radius * Math.cos(t));
        y = (float) (radius * Math.sin(t));
    }

    /**
     * Calcule le carrè de la distance du vecteur courant à un autre vecteur.
     *
     * @param other le vecteur avec lequel calculer le carrè de la distance.
     * @return le carrè de la distance calculè.
     */
    public float getDistSqr(Vector2 other) {
        float dx = x - other.x;
        float dy = y - other.y;
        return dx * dx + dy * dy;
    }

    /**
     * Calcule la distance du vecteur courant avec un autre vecteur.
     *
     * @param other le vecteur avec lequel calculer la distance.
     * @return la distance calculèe
     */
    public float getDist(Vector2 other) {
        float dx = x - other.x;
        float dy = y - other.y;
        return (float) Math.sqrt(dx * dx + dy * dy);
    }

    /**
     * Calcule l'orientation (-1 ou +1) de la composante horizontale courante.
     *
     * @return +1 si la composante horizontale est positive ou nulle, -1 sinon.
     */
    public int getOrientX() {
        return x < 0 ? -1 : 1;
    }

    /**
     * Calcule l'orientation (-1 ou +1) de la composante verticale courante.
     *
     * @return +1 si la composante verticale est positive ou nulle, -1 sinon.
     */
    public int getOrientY() {
        return y < 0 ? -1 : +1;
    }

    /**
     * Calcule le vecteur d'orientation du vecteur courant.
     *
     * @return le vecteur dont les composantes horizontales et verticales
     * correspondent aux orientation horizontales et verticales courantes.
     */
    public Vector2 getOrient() {
        return newXY(getOrientX(), getOrientY());
    }

    /**
     * Ajoute un autre vecteur au vecteur courant.
     *
     * @param other le vecteur à ajouter.
     */
    public void add(Vector2 other) {
        this.x += other.x;
        this.y += other.y;
    }

    /**
     * Soustrait un autre vecteur au vecteur courant.
     *
     * @param other le vecteur à soustraire.
     */
    public void sub(Vector2 other) {
        this.x -= other.x;
        this.y -= other.y;
    }

    /**
     * Produit scalaire du vecteur courant avec un autre vecteur.
     *
     * @param other le vecteur avec lequel faire le produit scalaire.
     * @return le scalaire rèsultat du produit.
     */
    public float dot(Vector2 other) {
        return x * other.x + y * other.y;
    }

    /**
     * Transforme le vecteur courant en son opposè.
     */
    public void neg() {
        x = -x;
        y = -y;
    }

    /**
     * Transforme le vecteur courant en son miroir vertical.
     */
    public void negX() {
        x = -x;
    }

    /**
     * Transforme le vecteur courant en son miroir horizontal.
     */
    public void negY() {
        y = -y;
    }

    /**
     * Zoome le vecteur courant d'un certain facteur d'èchelle.
     *
     * @param factor facteur d'èchelle.
     */
    public void scale(float factor) {
        x *= factor;
        y *= factor;
    }

    /**
     * Zoome le vecteur courant d'un facteur spècifique sur chaque composante.
     *
     * @param factorX facteur d'èchelle sur l'axe horizontal.
     * @param factorY facteur d'èchelle sur l'axe vertical.
     */
    public void scale(float factorX, float factorY) {
        x *= factorX;
        y *= factorY;
    }

    /**
     * Zoome le vecteur courant à partir d'un vecteur d'èchelle, en procèdant à
     * la multiplication composante à composante des deux vecteurs.
     *
     * @param other le vecteur d'èchelle à multiplier composante à composante.
     */
    public void scale(Vector2 other) {
        x *= other.x;
        y *= other.y;
    }

    /**
     * Fait une rotation du vecteur courant.
     *
     * @param degrees l'angle de rotation en degrès.
     */
    public void rotate(float degrees) {
        double rad = Math.toRadians(degrees);
        double cos = Math.cos(rad);
        double sin = Math.sin(rad);
        float z;
        z = (float) (x * cos - y * sin);
        y = (float) (x * sin + y * cos);
        x = z;
    }

    @Override
    public Vector2 clone() {
        return newXY(x, y);
    }

    /**
     * Donne une reprèsentation sous forme de chaïne des composantes
     * cartèsiennes du vecteur courant.
     *
     * @return
     */
    @Override
    public String toString() {
        return "(" + x + "," + y + ")";
    }

    /**
     * Transforme le vecteur courant en une copie d'un autre vecteur.
     *
     * @param other
     */
    public void copy(Vector2 other) {
        this.x = other.x;
        this.y = other.y;
    }
}
