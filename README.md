# Asteroids

Asteroids est un jeu où l’on déplace un vaisseau afin d’éviter des astéroïdes qui viennent vers nous. On peut se déplacer dans un espace défini par un rectangle en bas de la fenêtre de notre jeu. On ne se déplace que verticalement et horizontalement. Puisque l’on ne peut pas toujours éviter les astéroïdes, vous pouvez tirer en ligne droite des projectiles à l’infini avec le vaisseau. Lorsque vous touchez un astéroïdes votre vaisseau perd 1% de vie. Nous avons inclus des bonus dans le jeu qui n’étant pas présent dans le TP. Si vous tirez trois fois sur une planète rouge vous obtiendrez pendant quelque temps un bonus qui vous permettra de tirer trois projectiles en même temps. Si vous tirez trois fois sur une planète verte vous obtiendrez le bonus laser durant un temps limité. Enfin, si vous tirez trois fois sur une planète bleue, 5% de vie sera rendue à votre vaisseau. Le but est de survivre le plus longtemps possible, vous perdez lorsque votre vaisseau atteint 0%.

## Installation

Pour installer les sources du jeu, suivez les informations suivantes :

```
git clone https://gitlab.com/quentin10000/asteroids.git
```

Puis importez le dossier dans Eclipse : Import -> General -> Existing Project into Workspace

## Utilisation

Lancer la classe principal qui ce trouve dans `main\Main.java`

- Flèches directionnelles : déplacer le vaisseau
- Espace : tirer
- Echap : Ouvre le menu de pause

## Images

![Screenshot2](Astéroïds_GIT_PERSO/lib/img/Screenshot_11.png "Menu principal Asteroids")

![Screenshot](Astéroïds_GIT_PERSO/lib/img/Screenshot_10.png "Screenshot Asteroids")

## Bibliothèques

* [LWJGL](http://legacy.lwjgl.org/index.php.html) - Java Game Library
* [Slick2D](http://slick.ninjacave.com/) - 2D Java Game Library

